from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from .models import BaseQuestions,MCQQuestions,MAQQuestions,Question
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType


class BaseQuestionsSerializer(ModelSerializer):
    author = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CurrentUserDefault())
    class Meta:
        model = BaseQuestions
        fields = "__all__"

class QuestionSerializer(ModelSerializer):
    # object_id = serializers.SerializerMethodField()
    question = serializers.SerializerMethodField()

    class Meta:
        model = Question
        fields = "__all__"

    def get_question(self, obj):
        content_object = obj.content_object
        
        if isinstance(content_object, MAQQuestions):
            serializer = MAQQuestionsSerializer(content_object)

        elif isinstance(content_object, MCQQuestions):
            serializer = MCQQuestionsSerializer(content_object)
        else:
            raise Exception('Unexpected type of tagged object')

        return serializer.data




class MCQQuestionsSerializer(ModelSerializer):
    author = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CurrentUserDefault())
    generic_question_id = serializers.SerializerMethodField()

    class Meta:
        model = MCQQuestions
        fields = "__all__"

    def create(self, validated_data):
        mcq_question = MCQQuestions.objects.create(**validated_data)
        Question.objects.create(content_object=mcq_question)
        return mcq_question

    def get_generic_question_id(self, obj):
        question_type = ContentType.objects.get_for_model(obj)
        return Question.objects.get(
            object_id=obj.id, question_type=question_type).id



class MAQQuestionsSerializer(ModelSerializer):
    author = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CurrentUserDefault())
    class Meta:
        model = MAQQuestions
        fields = "__all__"
        # exclude = ("id",)

    def create(self, validated_data):
        maq_question = MAQQuestions.objects.create(**validated_data)
        Question.objects.create(content_object=maq_question)
        return maq_question





