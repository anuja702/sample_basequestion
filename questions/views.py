from rest_framework.viewsets import ModelViewSet
from .models import BaseQuestions,MAQQuestions,MCQQuestions,Question
from .serializers import BaseQuestionsSerializer,MCQQuestionsSerializer,MAQQuestionsSerializer,QuestionSerializer
from rest_framework.mixins import (
    DestroyModelMixin,UpdateModelMixin,
    ListModelMixin, CreateModelMixin, RetrieveModelMixin)
from rest_framework.exceptions import ValidationError

# class BaseQuestionsViewSet(ModelViewSet):
#     serializer_class = BaseQuestionsSerializer
#     queryset = BaseQuestions.objects.all()
#     filter_fields = ('author', )

class MAQQuestionsViewSet(ModelViewSet):
    serializer_class = MAQQuestionsSerializer
    queryset = MAQQuestions.objects.all()
    filter_fields = ('author', )

class MCQQuestionsViewSet(ModelViewSet):
    serializer_class = MCQQuestionsSerializer
    queryset = MCQQuestions.objects.all()
    filter_fields = ('author', )

class QuestionViewSet(ModelViewSet) :
    # serializer_class = QuestionSerializer
    queryset = Question.objects.all()
    filter_fields = ('question_type', )

    def get_serializer_class(self):
        # import ipdb; ipdb.set_trace()
        if self.action == "create" and self.request.data.get("question_type_name", None) == "MAQ":
            return MAQQuestionsSerializer
        elif self.action == "create" and self.request.data.get("question_type_name", None) == "MCQ":
            return MCQQuestionsSerializer
        elif self.action == "create":
            raise ValidationError("question_type_name not provided")
        return QuestionSerializer



# class CommentViewSet(ModelViewSet):
#     serializer_class = CommentSerializer
#     queryset = Comment.objects.all()

# class LikeViewSet(ModelViewSet):
#     serializer_class = LikesSerializer
#     queryset = Likes.objects.all()
