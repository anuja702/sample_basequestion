from django.contrib import admin

# Register your models here.
from .models import BaseQuestions,MCQQuestions,MAQQuestions,Question

# admin.site.register(BaseQuestions)
admin.site.register(MCQQuestions)
admin.site.register(MAQQuestions)
admin.site.register(Question)