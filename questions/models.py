from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
# Create your models here.
class BaseQuestions(models.Model) :
	text = models.TextField()
	title_external = models.CharField(max_length = 20)
	title_internal = models.CharField(max_length = 20)
	author = models.ForeignKey(User)

	class Meta:
		abstract = True

class Question(models.Model) : 
	question_type = models.ForeignKey(ContentType, related_name="question")
	object_id = models.PositiveIntegerField()
	content_object = GenericForeignKey('question_type', 'object_id')

class MCQQuestions(BaseQuestions) :
	text = models.TextField()
	mcq_field_1 = models.CharField(max_length = 20)
	mcq_field_2 = models.CharField(max_length = 20)
	author = models.ForeignKey(User)
	tags = GenericRelation(Question)

class MAQQuestions(BaseQuestions) :
	text = models.TextField()
	maq_field_1 = models.CharField(max_length = 20)
	mcq_field_2 = models.CharField(max_length = 20)
	author = models.ForeignKey(User)
	tags = GenericRelation(Question)

