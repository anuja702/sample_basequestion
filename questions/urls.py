from rest_framework.routers import SimpleRouter
from .views import QuestionViewSet,MCQQuestionsViewSet,MAQQuestionsViewSet
# from django.conf.urls import url

router = SimpleRouter()
router.register('questions', QuestionViewSet)
router.register('mcq', MCQQuestionsViewSet)
# router.register('basequestions', BaseQuestionsViewSet)
router.register('maq', MAQQuestionsViewSet)

urlpatterns = router.urls

# urlpatterns =[
#     url(r"^posts/", PostViewSet.as_view({"delete": "list"}))
# ]