from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from .models import BaseQuestions


class BaseQuestionsSerializer(ModelSerializer):
    author = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CurrentUserDefault())
    
    class Meta:
        model = BaseQuestions
        fields = "__all__"