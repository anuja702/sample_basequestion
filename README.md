# Questions

* create basequestion
* create MCQQuestion
* create MAQQuestion
* create Question

Create End Points for adding a question, listing and filtering questions based on question type

    GET questions/?question_type='MCQ'
    GET questions/?question_type='MAQ'
    POST questions/